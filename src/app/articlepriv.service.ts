import { Injectable } from '@angular/core';
import { Article } from './article';

import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ArticleprivService {

  
    headers= new HttpHeaders({ 'Content-Type': 'application/json' })
  
  private articlesUrl: string = 'http://localhost:4200/';  // URL to web api

  constructor(private http: HttpClient) { }

  getArticles() {
     
    return this.http.get(`${this.articlesUrl + 'listeArticle'+ '/listeArticle'}`);
  }

  /*getArticleById(articleId: number){
    return this.http.get(`${this.articlesUrl + 'articles'}/${articleId}`);
  }*/

  //
  //PAS BON DU TOUT, A SUPPRIMER
  //
  /*getArticlesFrom(auteur: string) {
    //if(!auteur){return of([]);}
    const options = auteur ?
   { params: new HttpParams().set('auteur', auteur) } : {};
    return this.http.get<Article[]>(`${this.articlesUrl}`, options);
    return this.http.get(`${this.articlesUrl + 'listeArticle' + '/listeArticle'}/${auteur}`);
  }*/

  /*ajouterArticle(article: Article): Observable<Article> {
    return this.http.post<Article>(this.articlesUrl, article,this.httpOptions);
  }*/

  /*deleteArticle(article: Article | number): Observable<Article> {
    const id = typeof article === 'number' ? article : article.id;
    const url = `${this.articlesUrl+ 'listeArticle'+ '/listeArticle'}/${id}`;

    return this.http.delete<Article>(url, this.httpOptions);
  }*/

  delete(id: number): Promise<void> {
    const url = `${this.articlesUrl+ 'listeArticle'+ '/listeArticle'}/${id}`;
    return this.http.delete(url, { headers: this.headers })
        .toPromise()
        .then(() => null);
  }

  

}
