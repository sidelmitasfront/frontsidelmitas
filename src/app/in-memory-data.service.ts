import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Article } from './article';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService{

  constructor() {}

  createDb() {
    const listeArticle =  [
      { id: 1,
          titre: 'La banquise est blanche',
          contenu: '1Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.',
          auteur: 'Paul Aire',
          date: '12/11/2010',
          comments: [
              { 
                  user: 'M. Manchot',
                  commentaire: 'En effet, elle l\'est depuis des années'
              },
              { 
                user: 'M. Ours',
                commentaire: 'Comme moi'
              },
              { 
                user: 'Mme. Phoque',
                commentaire: 'Toujours aussi belle! 🥰'
              }
          ] },
      { id: 2,
          titre: '50 nuances de Freud',
          contenu: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.',
          auteur: 'J.K. Running',
          date: '12/12/2012',
          comments: [
              { 
                  user: 'M. Grey',
                  commentaire: 'Je préfère de loin ma version'
              },
              { 
                user: 'M. Grey',
                commentaire: 'D\'ailleurs, tu veux jouer avec moi?'
              }
          ] },
      { id: 3,
          titre: 'La banquise fond',
          contenu: '2Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.',
          auteur: 'Paul Aire',
          date: '13/12/2011',
          comments: [
              { 
                  user: 'M. Ours',
                  commentaire: '😨😨'
              }
          ] }, 
      { id: 4,
          titre: 'La banquise est plus là',
          contenu: '3Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.',
          auteur: 'Paul Aire',
          date: '31/12/2019',
          comments: [
              { 
                  user: 'M. Manchot',
                  commentaire: 'RIP'
              },
              { 
                user: 'M. Ours',
                commentaire: 'Fait bien trop chaud aussi...🥵 Il y a déjà des krokus en Russie.'
              }
          ] },
      { id: 5,
          titre: '10 recettes minceur vegans',
          contenu: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.',
          auteur: 'M. Génicoco',
          date: '10/11/2019',
          comments: [
            { 
                user: 'Gordon Ramsay',
                commentaire: 'Trop cuite la viande. Ca a l\'air bien trop sec... Vilain petit sandwich!'
            }
        ] },
      { id: 6,
          titre: 'La banquise revient',
          contenu: '4Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.',
          auteur: 'Paul Aire',
          date: '01/01/2020',
          comments: [
            { 
                user: 'M. Ours',
                commentaire: 'Hourra'
            }
        ] },
      { id: 7,
          titre: 'Celsius 37',
          contenu: '5Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.',
          auteur: 'Thomas Momètre',
          date: '12/11/2010' },
      { id: 8,
          titre: 'Les oiseaux de nos jardins',
          contenu: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus.Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.',
          auteur: 'Alfred Camu',
          date: '26/03/1997' },
      { id: 9,
          titre: 'Les Fake News',
          contenu: '6Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.',
          auteur: 'DonaldDuck',
          date: '12/11/2010',
          comments: [
            { 
                user: 'M. Ours',
                commentaire: 'Pour une fois qu\'il écrit pas un tweet celui-là...'
            },
            { 
                  user: 'Mme. Phoque',
                  commentaire: 'Bien vrai!'
            }
          ] },
      { id: 10,
          titre: 'Adolescence: La fin d\'un monde?',
          contenu: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.',
          auteur: 'J.K. Running',
          date: '28/02/2010',
          comments: [
            { 
                user: 'Mme. Phoque',
                commentaire: 'Au moins quand ils deviennent ados, on ne les entend plus lors des trajets en voiture! Ils sont trop occupés sur leurs téléphones.'
            }
        ] },
      { id: 11,
          titre: 'La mécanique des fluides',
          contenu: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, ex, recusandae. Facere modi sunt, quod quibusdam.',
          auteur: 'J. Tuning',
          date: '03/09/2017' },
    
    ];
    const users = [
      { id: 1, pseudo: 'Paul Aire', email:"PaulAire@hotmail.be",password:"secret"},
      { id: 2, pseudo: 'Narcos',email:"MexicoPablo@hotmail.fr",password:"C@rtel25"},
      { id: 3, pseudo: 'M. Manchot', email:"Manchot.Empereur@gmail.be",password:"machot5"},
      { id: 4, pseudo: "M. Ours", email:"PolarBear@hotmail.na",password:"WhiteVolibear"},
      { id: 5, pseudo: "M. Grey",email:"MGrey.bandage@outlook.gb",password:"Megalovania"},
      { id: 6, pseudo: "Mme. Phoque",email:"MmePA.natation@outlook.com",password:"phoque1stAnimal"},
      { id: 7, pseudo: "J.K. Runnig",email:"J.K.Runnig@skynet.gb",password:"Read4ever"},
      { id: 8, pseudo: "M. Génicoco",email:"DocGenicoco@skynet.it",password:"veganBoy"},
      { id: 9, pseudo: "Gordon Ramsay",email:"G.Ramsay@skynet.na",password:"MeatLover"},
      { id: 10, pseudo: "Thomas Momètre",email:"TMomètre@hotmail.fr",password:"therMomètre"},
      { id: 11, pseudo: "Alfred Camu",email:"philoACamus@outlook.fr",password:"FreudLoser"},
      { id: 12, pseudo: "DonaldDuck",email:"Fakenews@outlook.na",password:"AmericaGreatAgain"},
      { id: 13, pseudo: "J. Tuning",email:"physiqueChimie.prof@helha.be",password:"ScienceMere2toutesChoses"},
    ];
  return{listeArticle, users};
  }

  //S'arrurer qu'on gen bien un id
  //Si vide -> id=1
  //Si pas -> plus grand id+1
  /*genId(datas: any[]): number {
    return datas.length > 0 ? Math.max(...datas.map(data => data.id)) + 1 : 1;
  }*/

}
