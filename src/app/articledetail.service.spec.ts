import { TestBed } from '@angular/core/testing';

import { ArticledetailService } from './articledetail.service';

describe('ArticledetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArticledetailService = TestBed.get(ArticledetailService);
    expect(service).toBeTruthy();
  });
});
