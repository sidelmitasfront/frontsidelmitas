import { Injectable, ɵɵloadContentQuery } from '@angular/core';
import { Article } from './article';

import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AddarticleService {

  
  private headers= new HttpHeaders({ 'Content-Type': 'application/json' });
  
  private articlesUrl: string = 'http://localhost:4200/';  // URL to web api


  constructor(private http: HttpClient) { }

  ajouterArticle(titre: string, contenu: string, auteur: string, date: string): Promise<Article> {
    //return this.http.post<Article>(this.articlesUrl, article, this.httpOptions);
    
    return this.http.post(`${this.articlesUrl + 'listeArticle'+ '/listeArticle'}`, {titre: titre,contenu: contenu,auteur: auteur,date : date} , {headers: this.headers})
    .toPromise()
    .then(res => res as Article);
  }

}
