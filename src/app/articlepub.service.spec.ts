import { TestBed } from '@angular/core/testing';

import { ArticlepubService } from './articlepub.service';

describe('ArticlepubService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArticlepubService = TestBed.get(ArticlepubService);
    expect(service).toBeTruthy();
  });
});
