import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'Sidelmitas';

  constructor(private cookieservice:CookieService,  private router: Router){}

  ngOnInit() {
    
  }

  deconect(){
    this.cookieservice.delete('isConnected');
    this.cookieservice.delete('pseudo');
    this.cookieservice.deleteAll();
    this.router.navigate(['/connexion']);
  }
}
