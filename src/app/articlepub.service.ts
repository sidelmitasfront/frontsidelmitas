import { Injectable } from '@angular/core';
import { Article } from './article';

import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ArticlepubService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  private articlesUrl: string = 'http://localhost:4200/';  // URL to web api

  constructor(private http: HttpClient) { }

  getArticles() {
     
    return this.http.get(`${this.articlesUrl + 'listeArticle'+ '/listeArticle'}`);
  }

  

}
