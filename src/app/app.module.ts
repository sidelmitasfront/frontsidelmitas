
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule } from '@angular/forms';
import { Route, RouterModule } from '@angular/router';
import { HttpClientModule }    from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { LOCALE_ID } from '@angular/core';
import { SecurityGuard } from './security.guard';
import { CookieService } from 'ngx-cookie-service';

import { ListeArticlePriveeComponent } from './component/liste-article-privee/liste-article-privee.component';
import { ListeArticlePublicComponent } from './component/liste-article-public/liste-article-public.component';
import { ArticleComponent } from './component/article/article.component';
import { AjoutArticleComponent } from './component/ajout-article/ajout-article.component';
import { ConnexionComponent } from './component/connexion/connexion.component';
import { InscriptionComponent } from './component/inscription/inscription.component';
import { MdpOublieComponent } from './component/mdp-oublie/mdp-oublie.component';



registerLocaleData(localeFr, 'fr');

const appRoute: Route[] = [
  { path: 'inscription', component: InscriptionComponent },
  { path: 'connexion', component: ConnexionComponent },
  { path: 'ajoutArticle', component: AjoutArticleComponent, canActivate: [SecurityGuard] },
  { path: 'mdpOublie', component: MdpOublieComponent },
  { path: 'article/:idArticle', component: ArticleComponent, canActivate: [SecurityGuard] }, // vue d'un seul article
  { path: 'listeArticle', component: ListeArticlePublicComponent, canActivate: [SecurityGuard] }, // liste des articles publique
  { path: 'listeArticle/:pseudo', component: ListeArticlePriveeComponent, canActivate: [SecurityGuard] }, // liste des articles privé
//  { path: '', redirectTo: 'connexion', pathMatch: 'full' },
  { path: '**', redirectTo: 'listeArticle', pathMatch: 'full' },
];
@NgModule({
  declarations: [
    AppComponent,
    ListeArticlePriveeComponent,
    ListeArticlePublicComponent,
    ArticleComponent,
    AjoutArticleComponent,
    ConnexionComponent,
    InscriptionComponent,
    MdpOublieComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    RouterModule.forRoot(appRoute),
    HttpClientModule,
    //Un semblant de server/API, en mémoire
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }),
    

  ],
  providers: [{ provide: LOCALE_ID, useValue: "fr" }, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
