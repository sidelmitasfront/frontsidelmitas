import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class SecurityGuard implements CanActivate {
  constructor(private cookieService: CookieService, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (next.routeConfig.path === 'connexion') {
      if (!(this.cookieService.check('isConnected') && this.cookieService.get('isConnected') === '1')) {
        return true;
      } else {
        this.router.navigate(['listeArticle']);
        return false;
      }
    } else {
      if (this.cookieService.check('isConnected') && this.cookieService.get('isConnected') === '1') {
        return true;
      } else {
        this.router.navigate(['connexion']);
        return false;
      }
    }
  }
}
