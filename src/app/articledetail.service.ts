import { Injectable } from '@angular/core';
import { Article } from './article';
import {  map, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class ArticledetailService {

  
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  private articlesUrl: string = 'http://localhost:4200/';  // URL to web api


  constructor(private http: HttpClient) { }

  getArticle(id: number){
    return this.http.get(`${this.articlesUrl + 'article' + '/listeArticle'}/${id}`);
    //return this.http.get<Article>(`${this.articlesUrl + 'article' + '/listeArticle'}/${id}`);
  }
  

  /*updateArticle(article: {id: number, titre: string, contenu: string, auteur: string, date: string}){
    return this.http.put(`${this.articlesUrl + 'article' + '/listeArticle'}/${article.id}`, article);
  }*/

  /*updateArticle(article: Article): Observable<Article>{
    return this.http.put<Article>(`${this.articlesUrl + 'article' + '/listeArticle'}/${article.id}`, {id: article.id,titre: article.titre,contenu: article.contenu,auteur: article.auteur,date : article.date}, this.httpOptions )
    .pipe(tap(data => console.log(data)));
  }*/

  addArticleUp(id: number, titre: string, contenu: string, auteur: string, date: string): Promise<Article> {
    //return this.http.post<Article>(this.articlesUrl, article, this.httpOptions);
    return this.http.post(`${this.articlesUrl + 'article'+ '/listeArticle'}`,
     { titre: titre,contenu: contenu,auteur: auteur,date : date},
      this.httpOptions)
    .toPromise()
    .then(res => res as Article);
  }
  deleteUp(id: number): Promise<void> {
    const url = `${this.articlesUrl+ 'listeArticle'+ '/listeArticle'}/${id}`;
    return this.http.delete(url, this.httpOptions)
        .toPromise()
        .then(() => null);
  }


  updateComments(article: {id: number}, user: string, commentaire: string){
    return this.http.put(`${this.articlesUrl + 'article' + '/listeArticle'}/${article.id}`, article);
  }

  /*deleteArticle(article: Article | number): Observable<Article> {
    const id = typeof article === 'number' ? article : article.id;
    const url = `${this.articlesUrl+ 'listeArticle'+ '/listeArticle'}/${id}`;

    return this.http.delete<Article>(url, this.httpOptions);
  }*/

  getArticles() {
     
    return this.http.get(`${this.articlesUrl + 'listeArticle'+ '/listeArticle'}`);
  }
}
