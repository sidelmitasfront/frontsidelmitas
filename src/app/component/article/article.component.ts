import { Component, OnInit } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { Article } from '../../article';
import { ArticledetailService } from '../../articledetail.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import {formatDate} from '@angular/common';
import { identifierModuleUrl } from '@angular/compiler';


@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  cookieValue: string = "";
  articleToDisplay: any;
  articleUp: any;
  articles: any[] = [];
  ident: any;
 

  textLike: string = 'J\'aime 🤍';
  comments = [];

  commentToAdd: {
        id: number;
        user: string;
        comment: string;
      } = {id: 0, user: undefined, comment: undefined};

  constructor(private cookieService:CookieService, private articledetailService: ArticledetailService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      var id = params.get('idArticle');
      this.ident = id;
    this.getArticle(id);
  });
  }

  getArticle(articleId): void{
    
    this.articledetailService.getArticle(articleId).subscribe(data =>{
      this.articleToDisplay = data;
      
      if(this.cookieService.get('pseudo')==this.articleToDisplay.auteur){
        this.cookieValue="isAuteur";
      }
    } );
    
  }
  

  addComment(){
    if(this.commentToAdd.comment){
      this.commentToAdd.id = this.comments.length + 1;
      this.commentToAdd.user = "Vous";
      this.comments.push(this.commentToAdd);
      this.commentToAdd = {id: 0, user: undefined, comment: undefined};
    }
  }

  public changeLike() {
    if(this.textLike === 'J\'aime 🤍') { 
      this.textLike = 'J\'aime ❤️'
    } else {
      this.textLike = 'J\'aime 🤍'
    }
  }

  /*saveChanges(titre, contenu){
    this.articledetailService.getArticle(this.ident)
    .subscribe(data => {
      this.articleUp = data;
      this.articledetailService.updateArticle(this.articleUp as Article)
      .subscribe(data1 =>{
        this.getArticles();
      })
    })
  }*/
  saveChanges(titre, contenu){
    if(!titre || !contenu){ return;}
    var date = formatDate(new Date(), 'dd/MM/yyyy', 'fr');
    var auteur= this.cookieService.get('pseudo');
    var id= this.ident;
    var titre = titre;
    var contenu = contenu;
    this.articledetailService.addArticleUp(id, titre, contenu, auteur, date)
      .then(article =>{
        this.articles.push(article)
      });
    this.articledetailService.deleteUp(id)
    .then(() => {
      this.articles = this.articles.filter(h => h !== this.articleToDisplay);
    });
    
    this.router.navigate(['/listeArticle']);
  }

  getArticles(): void {
    
    this.articledetailService.getArticles().subscribe((data: any[])=>{
      this.articles = data;
    })
  }

  /*saveChanges(articleToDisplay2: {id: number, titre: string, contenu: string, auteur: string, date: string}): void{
    if(!articleToDisplay2.titre || !articleToDisplay2.contenu){ return;}
    var date = formatDate(new Date(), 'dd/MM/yyyy', 'fr');
    var auteur= this.cookieService.get('pseudo');
    var id= articleToDisplay2.id;
    var titre = articleToDisplay2.titre;
    var contenu = articleToDisplay2.contenu;
    let art: {id: number, titre: string, contenu: string, auteur: string, date: string} = {
      id,
      titre,
      contenu,
      auteur,
      date
    }
    
    this.articledetailService.updateArticle(art).subscribe(data =>{
      console.log("Policy updated: ", data);
      this.articleToDisplay = data;
      
      
    } );
    /*.then(article =>{
      console.log(article.titre);
      this.articleToDisplay.push(article);
    });
    this.router.navigate(['/listeArticle']);
  }*/

}
