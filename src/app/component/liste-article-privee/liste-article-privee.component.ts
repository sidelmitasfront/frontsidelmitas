import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { Article } from '../../article';
import { ArticleprivService } from '../../articlepriv.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { CookieService } from 'ngx-cookie-service';



@Component({
  selector: 'app-liste-article-privee',
  templateUrl: './liste-article-privee.component.html',
  styleUrls: ['./liste-article-privee.component.scss']
})
export class ListeArticlePriveeComponent implements OnInit {
  
  articles: any[] = [];

  constructor(private articleprivService: ArticleprivService, private route: ActivatedRoute, private cookieservice:CookieService) { }

  ngOnInit() {
    /*this.route.paramMap.subscribe(params => {
      //var pseu = params.get('pseudo');
      var pseu = this.cookieservice.get('pseudo');
    
    this.getArticlesFrom(pseu);
  });*/
    var pseu = this.cookieservice.get('pseudo');
    this.getArticlesFrom(pseu);
  }

  //Pas besoin ici en fait
  getArticles(): void {
    
    this.articleprivService.getArticles().subscribe((data: any[])=>{
      this.articles = data;
    })
  }

  /*getArticleById(param): void{
    this.articleprivService.getArticleById(param).subscribe((data: any[])=>{
      this.articles = data;
    })
  }*/

  getArticlesFrom(pseudo): void {
    this.articleprivService.getArticles().subscribe((data: any[])=>{
      
      const tab = [];
      data.forEach(function(article){
        
        if(article.auteur==pseudo){
         
          tab.push(article);
          //console.log(article);
        }
        
      });
      this.articles = tab;
    });
    
  }

  /*ajouter(titre: string, contenu: string): void{
    if(!titre || !contenu){ return;}
    let article: Article = {
      titre: titre,
      contenu: contenu,
      auteur: 'Paul Aire',
      date: '12/11/2010'};

    this.articleprivService.ajouterArticle( article as Article).subscribe(article => {this.articles.push(article)});
  }*/
 
  /*deleteArticle(article: Article): void {
    this.articles = this.articles.filter(h => h !== article);
    this.articleprivService.deleteArticle(article).subscribe();
  }*/

  delete(article: Article): void {
    this.articleprivService
        .delete(article.id)
        .then(() => {
          this.articles = this.articles.filter(h => h !== article);
        
        });
  }


}
