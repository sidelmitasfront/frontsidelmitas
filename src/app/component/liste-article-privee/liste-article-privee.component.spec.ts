import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeArticlePriveeComponent } from './liste-article-privee.component';

describe('ListeArticlePriveeComponent', () => {
  let component: ListeArticlePriveeComponent;
  let fixture: ComponentFixture<ListeArticlePriveeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeArticlePriveeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeArticlePriveeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
