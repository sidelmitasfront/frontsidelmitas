import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../user.service';
import { CookieService } from 'ngx-cookie-service';
import { User } from '../../user';


@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent implements OnInit {
  //userToConnect: { pseudo?: string; password?: string } = {};
  errorMessage: string = "";
  users: any[] = [];

  constructor(private userservice: UserService, private cookieService:CookieService,  private router: Router) {}

  ngOnInit() {
    this.loadUsers();
  }

  loadUsers(){
    this.userservice.getUsers().then(users => this.users = users);
    this.cookieService.delete('isConnected');
      this.cookieService.delete('pseudo');
      this.cookieService.deleteAll();
    //console.log(this.users);
  }

  signin(pseudo, mdp): void{
      this.loadUsers();
      this.cookieService.delete('isConnected');
      this.cookieService.delete('pseudo');
      this.cookieService.deleteAll();
    
    this.userservice.getUsers()
    .then(users => this.users = users);

    this.users.forEach(user =>{
      //let erreur = 0;
      if(user.pseudo==pseudo){
        if(user.password==mdp){
          
          this.cookieService.set('isConnected', '1');
          this.cookieService.set('pseudo', pseudo);
          this.router.navigate(['/listeArticle']);
        }
        else{
           
          this.errorMessage = "Mauvais pseudo ou mot de passe";
        }
      }
      else{
        
        this.errorMessage = "Mauvais pseudo ou mot de passe";
      }
    });
    
  }

}