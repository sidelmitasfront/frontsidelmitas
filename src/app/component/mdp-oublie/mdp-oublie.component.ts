import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {UserService} from '../../user.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-mdp-oublie',
  templateUrl: './mdp-oublie.component.html',
  styleUrls: ['./mdp-oublie.component.scss']
})
export class MdpOublieComponent implements OnInit {

  user: any;
  errorMessage: string ="";
  users: any[] = [];
  constructor(private cookieService:CookieService, private router: Router, private userservice: UserService ) { }

  ngOnInit() {
    this.loadUsers();
  }

  loadUsers(){
    this.userservice.getUsers().then(users => this.users = users);
    this.cookieService.delete('isConnected');
      this.cookieService.delete('pseudo');
      this.cookieService.deleteAll();
    //console.log(this.users);
  }
  
    
  changePwd(pseudo, mdp, mdpconf){
    if(!pseudo || !mdp || !mdpconf){
      this.errorMessage = "Complétez tous les champs"
    }
    else{
      if(mdp === mdpconf){
        this.userservice.getUsers().then(users => this.users = users);
        this.users.forEach(user =>{
          var mail = user.email;
          var id = user.id;
          
          if(user.pseudo==pseudo){
            this.userservice.addUser(pseudo, mail, mdp)
            .then(user1 =>{
              this.users.push(user);
            });
            this.userservice.deleteUs(id)
            .then(() =>{
              this.users = this.users.filter(h => h !== this.user);
            });

              this.router.navigate(['/connexion']);
            
          }
          else{
            this.errorMessage = "Personne n'a ce pseudo";
          }
        });
      }
      else{
        this.errorMessage="Le champ mot de passe et sa confirmation ne sont pas identiques"
      }
    }

    /*this.service.updatePassword(this.user).subscribe(       
      result => {
         this.errorMessage = undefined;
         this.router.navigate(['connexion']);
       },
       error => {
         this.errorMessage = error.error.result;
       }
     );*/
  }
}