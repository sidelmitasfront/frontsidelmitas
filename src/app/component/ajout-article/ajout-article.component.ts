import { Component, OnInit } from '@angular/core';
import { Article } from '../../article';
import { AddarticleService } from '../../addarticle.service';
import {formatDate} from '@angular/common';
import { Router } from "@angular/router";
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-ajout-article',
  templateUrl: './ajout-article.component.html',
  styleUrls: ['./ajout-article.component.scss']
})
export class AjoutArticleComponent implements OnInit {

  constructor(private cookieservice:CookieService, private addarticleService: AddarticleService, private router: Router) { }

  articles: any[] = [];

  ngOnInit() {
  }

  ajouter(titre: string, contenu: string): void{
    if(!titre || !contenu){ return;}
    var date = formatDate(new Date(), 'dd/MM/yyyy', 'fr');
    var auteur= this.cookieservice.get('pseudo');
    
    //this.addarticleService.ajouterArticle( article as Article).subscribe(article => {this.articles.push(article)});
      this.addarticleService.ajouterArticle(titre, contenu, auteur, date)
      .then(article =>{
        this.articles.push(article)
      });

    this.router.navigate(['/listeArticle']);
  }

}
