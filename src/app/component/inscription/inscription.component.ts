import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }

  errorMessage: string = "";
  users: any[] = [];

  ngOnInit() {
  }

  saveAccount(pseudo, email, mdp, mdpconf): void{
    if(!pseudo || !email || !mdp || !mdpconf){
      this.errorMessage = "Complétez tous les champs"
    }
    else{ //tout complet
      if(mdp === mdpconf){
        this.userService.addUser(pseudo, email, mdp)
        .then(user =>{
          this.users.push(user)
        });
        this.router.navigate(['/connexion']);
      }
      else{
        this.errorMessage="Le champ mot de passe et sa confirmation ne sont pas identiques"
      }
      
    }
  }

}
