import { Component, OnInit } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { Article } from '../../article';
import { ArticlepubService } from '../../articlepub.service';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-liste-article-public',
  templateUrl: './liste-article-public.component.html',
  styleUrls: ['./liste-article-public.component.scss']
})
export class ListeArticlePublicComponent implements OnInit {

  articles: any[] = [];

  constructor(private articlepubService: ArticlepubService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getArticles();
  }

  getArticles(): void {
    
    this.articlepubService.getArticles().subscribe((data: any[])=>{
      this.articles = data;
    })
  }

}
