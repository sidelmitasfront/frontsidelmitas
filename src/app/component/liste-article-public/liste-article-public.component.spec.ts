import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeArticlePublicComponent } from './liste-article-public.component';

describe('ListeArticlePublicComponent', () => {
  let component: ListeArticlePublicComponent;
  let fixture: ComponentFixture<ListeArticlePublicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeArticlePublicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeArticlePublicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
