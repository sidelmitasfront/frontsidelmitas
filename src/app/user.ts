export interface User {
    id?: number;
    pseudo?: string;
    email?: string;
    password?: string; // propriété facultative avec le '?'
  }
  