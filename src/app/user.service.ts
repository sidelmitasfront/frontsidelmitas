import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import {User} from './user';



@Injectable({
  providedIn: 'root'
})
export class UserService {
  private headers= new HttpHeaders({ 'Content-Type': 'application/json' });
  private usersUrl: string = 'http://localhost:4200/';

  constructor(private http: HttpClient) {}

  getUsers(): Promise<User[]>{
    
    const url = `${this.usersUrl+ 'article' + '/users'}`;
    return this.http.get(url)
    .toPromise()
    .then(response => response as User[]);
  }

  /*authenticate(userToConnect: { pseudo?: string; password?: string }){
     return this.http.get<User>(`${this.usersUrl+'article'+'/user'}/${userToConnect.pseudo}`);
    //return this.http.get<any>(Users + '?pseudo=' + userToConnect.pseudo + '&password=' + userToConnect.password);
  }*/
  updatePassword(userToChangePwd:{pseudo?: string; password?: string ; confPassword?:string}){
    if(userToChangePwd.password == userToChangePwd.confPassword)
    {
    return this.http.put(`${this.usersUrl+'article'+'/user'}/${userToChangePwd.pseudo}`,userToChangePwd);
    }
    else return null;
  }
  /*ajoutUser(user:{pseudo?: string; email?;string; password?: string}){
    return this.http.put(`${this.usersUrl+'article'+'/user'}/${user.pseudo}`,user);
  }*/

  addUser(pseudo: string, mail: string, mdp: string): Promise<User>{
    return this.http.post(`${this.usersUrl + 'article'+ '/users'}`, {pseudo: pseudo, email: mail,password: mdp} , {headers: this.headers})
    .toPromise()
    .then(res => res as User);
  }
  deleteUs(id: number): Promise<void> {
    const url = `${this.usersUrl+ 'article'+ '/users'}/${id}`;
    return this.http.delete(url, {headers: this.headers})
        .toPromise()
        .then(() => null);
  }

}