import { TestBed } from '@angular/core/testing';

import { ArticleprivService } from './articlepriv.service';

describe('ArticleprivService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArticleprivService = TestBed.get(ArticleprivService);
    expect(service).toBeTruthy();
  });
});
